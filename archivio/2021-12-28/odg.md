# Riunione eXt Dicembre

1. Fissare prossima riunione ed incaricare moderatrice/moderatore;
2. **Organizzazione ScienceAfterChristmas** 8-9 Gennaio [avanzamento lavori]
3. Riflessioni post-riunione di valutazione con ACDC [breve riassunto per chi non era presente][[verbale](https://demo.hedgedoc.org/iJvi1pj8Rduly1D2_gdaAg#)];
  - Status Zanzara e Arci Virgilio
  - Sito web
  - Radio
  - Eventi con ospiti
  - Chiusura bando MIUR
  - Varie ed eventuali
4. Brevi report sull’avanzamento dei progetti in corso. [Linguaggi, Via Fluminis, Zerounizzazione, non Ufficio Stampa, Libere Letture nelle scuole mantovane, laboratori alla fattoria (BO) e col master di studi di genere (RM), varie ed eventuali]
5. Possibile collaborazione con Lecturers Without Borders https://lewibo.org/
6. Raccolta idee per la partecipazione alla Giornata mondiale dell'acqua (Marzo 22, solo online)
